//#define BGACCENT "#ffb86c"
//#define BGACCENT "#ff79c6"
//#define BGACCENT "#bd93f9"
//#define BGACCENT "#d6acff"
//#define BGACCENT "#50fa7b"
//#define BGACCENT "#f1fa8c"
//#define BGACCENT "#8be9fd"
#define BGACCENT "#3b4048"

//#define FGACCENT "#1e1f29"
#define FGACCENT "#f8f8f2"

static char normbgcolor[]	= "#191A21";
static char normbordercolor[]	= "#1e1f29";
static char normfgcolor[]	= "#f8f8f2";
static char selbgcolor[]	= BGACCENT;
static char focus_br[]	= BGACCENT;
static char selbordercolor[]	= FGACCENT;
