#define ACCENTCOLOR "#b58900"
//#define ACCENTCOLOR "#859900"
//#define ACCENTCOLOR "#268bd2"
//#define ACCENTCOLOR "#6c71c4"
//#define ACCENTCOLOR "#d33682"
//#define ACCENTCOLOR "#eee8d5"

static char normfgcolor[]		= "#fdf6e3";
static char normbgcolor[]		= "#002b36";
static char normbordercolor[]		= "#002b36";
static char selbordercolor[]		= "#002b36";
static char selbgcolor[]		= ACCENTCOLOR;
static char focus_br[]		= ACCENTCOLOR;

//static char selbgcolor[]		= "#073642";
